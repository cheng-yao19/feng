
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>J&C</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="/dist/css/swiper.min.css">

    <!-- Demo styles -->
    <style>
        html, body {
            position: relative;
            height: 100%;
        }
        body {
            background: #eee;
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            font-size: 14px;
            color:#000;
            margin: 0;
            padding: 0;
        }
        .swiper-container {
            width: 100%;
            height: 100%;
            margin-left: auto;
            margin-right: auto;
        }
        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        .title {
            font-size: 84px;
            color: #ffffff;
        }
        body>nav.top {
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            text-transform: uppercase;
            font-weight: 800;
            font-size: .6em;
            padding: 0 32px;
            letter-spacing: 1px;
            z-index: 10000;
        }
        .clearfix:before, .clearfix:after {
            display: table;
            content: '';
        }
        body>nav.top span.right {
            float: right;
        }
        body>nav.top span.right a {
            display: block;
            float: left;
            color: #fff;
        }
        body>nav.top a {
            display: inline-block;
            color: #333;
            text-decoration: none;
            transform: translateY(20px);
        }
        a {
            outline: 0;
            color: #cf4a5c;
            text-decoration: none;
            transition: color .25s ease-in-out;
        }
        [class^="icon-"], [class*=" icon-"] {
            font-family: 'icomoon';
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .icon-earth:before {
            content: "\e605";
        }
        *, *:after, *:before {
            box-sizing: border-box;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
<!-- Swiper -->
<div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-color: rgba(240,128,128,0.6);">
            <nav class="top clearfix">
                <span class="right">
                    <a href="javascript:;" data-action="switch-language">
                        <span>
                            <i class="icon-earth"></i> login
                        </span>
                    </a>
                </span>
            </nav>
            <div class="title m-b-md">
                Hello
            </div>
        </div>
        <div class="swiper-slide" style="background-color: rgba(240,108,11,0.61)">
            <div class="title m-b-md">
                Welcome
            </div>
        </div>
        <div class="swiper-slide" style="background-color: rgba(156,202,184,0.61);">
            <div class="title m-b-md">
                to
            </div>
        </div>
        <div class="swiper-slide" style="background-color: rgba(240,198,216,0.6)">
            <div class="title m-b-md">
                My
            </div>
        </div>
        <div class="swiper-slide" style="background-color: rgba(186,168,193,0.6)">
            <div class="title m-b-md">
                J&C's
            </div>
        </div>
        <div class="swiper-slide" style="background-color: rgba(143,187,209,0.6)">
            <div class="title m-b-md">
                Website
            </div>
        </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>

<!-- Swiper JS -->
<script src="/dist/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper('.swiper-container', {
        direction: 'vertical',
        slidesPerView: 1,
        spaceBetween: 30,
        mousewheel: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
</script>
</body>
</html>
